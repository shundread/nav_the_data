Navigate The Data
===============

Entry in PyWeek #20  <http://www.pyweek.org/20/>
URL: https://www.pyweek.org/e/nav_the_data/
Team: shundread
Members: shundread
License: see LICENSE.txt


Running the Game
----------------

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py


How to Play the Game
--------------------

Navigate a world to which you have access to a wealth of information, presented to you in a very unhelpful manner.

Move your character with the arrow keys.

Press the left mouse button to fire the ducks.


Development notes 
-----------------

Creating a source distribution with::

   python setup.py sdist

You may also generate Windows executables and OS X applications::

   python setup.py py2exe
   python setup.py py2app

Upload files to PyWeek with::

   python pyweek_upload.py

Upload to the Python Package Index with::

   python setup.py register
   python setup.py sdist upload

