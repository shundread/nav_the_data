'''Floating-point Rectangles! Pygame, why don't you have that? :('''

class Rectangle(object):
    '''Floating-point Rectangles. Using X axis growing to the right and Y axis
    growing to the bottom, so I won't get confused dealing with Pygame.'''
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    @property
    def left(self):
        return self.x
    @left.setter
    def left(self, x):
        self.x = x

    @property
    def right(self):
        return self.x + self.w
    @right.setter
    def right(self, x):
        self.x = x - self.w

    @property
    def bottom(self):
        return self.y + self.h
    @bottom.setter
    def bottom(self, y):
        self.y = y - self.h

    @property
    def top(self):
        return self.y
    @top.setter
    def top(self, y):
        self.y = y

    @property
    def area(self):
        return self.w * self.h

    @classmethod
    def intersects(cls, r1, r2):
        '''True if r1 intersects r2. Being tangencial is not considered an
        intersection in this case.'''
        return not(r1.right  <= r2.left or
                   r1.left   >= r2.right or
                   r1.top    >= r2.bottom or
                   r1.bottom <= r2.top)

    @classmethod
    def intersection(cls, r1, r2):
        '''Returns a rectangle representing the intersection between r1 and r2
        or None if there is no intersection.'''
        left = max(r1.left, r2.left)
        right = min(r1.right, r2.right)
        if left >= right:
            return None
        top = max(r1.top, r2.top)
        bottom = min(r1.bottom, r2.bottom)
        if bottom <= top:
            return None
        return Rectangle(left, top, right-left, bottom-top)

    def __str__(self):
        return "{0}({1:.2f}, {2:.2f}, {3:.2f}, {4:.2f})".format(
                self.__class__.__name__, self.x, self.y, self.w, self.h)
