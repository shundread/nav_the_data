# Pygame imports
# ...

# Custom imports
from ..rectangle import Rectangle
from static import Goal, Wall


class Dynamic(Rectangle):
    def __init__(self, *args, **kargs):
        super(Dynamic, self).__init__(*args, **kargs)
        self.dx = 0
        self.dy = 0

    def calculate_next_movement(self, events, dt):
        pass

    def update(self, events, dt):
        self.calculate_next_movement(events, dt)

    def collide(self, thing):
        if isinstance(thing, Wall):
            return self.collide_wall(thing)
        if isinstance(thing, Goal):
            return self.collide_goal(thing)

    def collide_wall(self, wall):
        pass
