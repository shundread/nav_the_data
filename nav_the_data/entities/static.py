from ..rectangle import Rectangle

class Goal(Rectangle):
    solid = False

class Signal(Rectangle):
    solid = False

class Region(Rectangle):
    solid = False

    def __init__(self, text, *args, **kargs):
        super(Region, self).__init__(*args, **kargs)
        self.text = text

class Wall(Rectangle):
    solid = True

