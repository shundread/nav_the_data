'''This file defines the human-controlled player in the game.'''

# Pygame imports
import pygame
from pygame import key

# Custom lib imports
from dynamic import Dynamic

from ..rectangle import Rectangle
from ..event import GoalReached, PlayerDied

class Player(Dynamic):
    kspeed = 2.0

    def calculate_next_movement(self, events, dt):
        keys = key.get_pressed()
        self.dx = 0
        self.dy = 0

        # The player moves its size per second
        if keys[pygame.K_RIGHT]:
            self.dx += self.w * dt * Player.kspeed
        if keys[pygame.K_LEFT]:
            self.dx -= self.w * dt * Player.kspeed
        if keys[pygame.K_UP]:
            self.dy -= self.h * dt * Player.kspeed
        if keys[pygame.K_DOWN]:
            self.dy += self.h * dt * Player.kspeed

    def collide_wall(self, wall):
        super(Player, self).collide_wall(wall)

    def collide_goal(self, goal):
        print "Reaching goal"
        pygame.event.post(GoalReached())

