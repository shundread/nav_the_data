"This file describes a level"

import pygame
from pygame import Rect
from pygame import font

from rectangle import Rectangle

from entities.static import Goal, Region, Wall, Signal
from entities.player import Player

from engine import Scene

from views.matcher import DynamicMatcher, GoalMatcher, WallMatcher
from views.topological import TopologicalView
from views.histogram import HorizontalHistogramView, VerticalHistogramView
from views.region import RegionView

from views.matcher import WallMatcher, DynamicMatcher

#####################
# Utility functions #
#####################

def common_views(level):
    screen = pygame.display.get_surface()
    w, h = screen.get_size()

    # Histograms
    vwall_s = screen.subsurface(Rect(w*2/4, 0, w*1/4, h/2))
    vdyn_s  = screen.subsurface(Rect(w*3/4, 0, w*1/4, h/2))

    hwall_s = screen.subsurface(Rect(0, h*2/4, w*2/4, h*1/4))
    hdyn_s = screen.subsurface( Rect(0, h*3/4, w*2/4, h*1/4))

    # Region
    region_s = screen.subsurface(Rect(w*2/4, h*2/4, w*2/4, h*2/4))

    # Matchers
    wall_matcher = WallMatcher()
    dynamic_matcher = DynamicMatcher()

    # Fonts
    f = pygame.font.SysFont("arial", 20)

    views = [
        HorizontalHistogramView("Walls:", wall_matcher, 64, level.scene, hwall_s),
        HorizontalHistogramView("You:", dynamic_matcher, 64, level.scene, hdyn_s),
        VerticalHistogramView("Walls:", wall_matcher, 64, level.scene, vwall_s),
        VerticalHistogramView("You:", dynamic_matcher, 64, level.scene, vdyn_s),
        RegionView(f, level.scene, region_s)
    ]
    return views

def level_views(level):
    screen = pygame.display.get_surface()
    w, h  = screen.get_size()

    # Goal view
    goal_s = screen.subsurface(Rect(0, 0, w/2, h/2))
    extra_views = [TopologicalView(level.scene, goal_s)]

    return common_views(level) + extra_views

##########
# Levels #
##########

class Level1(object):
    def __init__(self):
        self.static = [
            # End position
            Goal(0.8, 0.0, 0.2, 0.2),

            # Walls
            Wall(0.4, 0.0, 0.2, 0.5),

            # Signal hotspots for topological view
            Signal(0.0, 0.0, 1.0, 1.0),

            # Regions
            Region(
                "Use the arrow keys to get to the green area",
                0.0, 0.0, 1.0, 1.0
            ),
        ]
        self.dynamic = [Player(0.0, 0.0, 0.2, 0.2),]
        self.scene = Scene(self.static, self.dynamic)
        self.views = level_views(self)

class Level2(object):
    def __init__(self):
        self.static = [
            # End position
            Goal(0.8, 0.0, 0.2, 0.2),

            # Walls
            Wall(0.4, 0.0, 0.2, 0.5),

            # Signal hotspots for topological view
            Signal(0.0, 0.0, 0.5, 0.5),

            # Regions
            Region(
                "Good job, now let's try this again",
                0.0, 0.0, 0.5, 0.3
            ),
            Region(
                "Uh oh. The map is broken, better\n"
                "rely on the more stable histogram\n"
                "views.",
                0.0, 0.6, 1.0, 0.4
            ),
            Region(
                "Almost there.",
                0.5, 0.0, 0.5, 0.3
            )
        ]
        self.dynamic = [Player(0.0, 0.0, 0.2, 0.2),]
        self.scene = Scene(self.static, self.dynamic)
        self.views = level_views(self)

class Level3(object):
    def __init__(self):
        self.static = [
            # End position
            Goal(0.0, 0.6, 0.2, 0.2),

            # Walls
            Wall(0.2, 0.0, 0.2, 0.2),
            Wall(0.0, 0.4, 0.4, 0.2),
            Wall(0.2, 0.6, 0.4, 0.2),
            Wall(0.8, 0.6, 0.2, 0.4),

            # Signal hotspots for topological view
            Signal(0.0, 0.0, 0.2, 0.2),
            Signal(0.0, 0.6, 0.4, 0.4),

            # Regions
            Region(
                "Remember to pay attention to this\n"
                "text-based view for useful data\n"
                "about the current room you're in.",
                0.0, 0.0, 0.4, 0.4
            ),
            Region(
                "A square-shaped room, with a door\n"
                "centered on the wall to the west\n"
                "and another door centered on the\n"
                "wall to the south.",
                0.4, 0.0, 0.6, 0.6
            ),
            Region(
                "An L-shaped corridor extending to\n"
                "the west, and turning right towards\n"
                "north.",
                0.0, 0.6, 1.0, 0.4
            ),
        ]
        self.dynamic = [Player(0.0, 0.0, 0.15, 0.15),]
        self.scene = Scene(self.static, self.dynamic)
        self.views = level_views(self)

class Level4(object):
    def __init__(self):
        self.static = [
            # End position
            Goal(0.0, 0.0, 0.2, 0.2),

            # Walls
            Wall(0.2, 0.0, 0.1, 0.8),
            Wall(0.3, 0.7, 0.5, 0.1),
            Wall(0.7, 0.2, 0.1, 0.5),
            Wall(0.5, 0.2, 0.2, 0.2),

            # Signal hotspots for topological view
            Signal(0.0, 0.0, 0.1, 1.0),

            # Regions
            Region(
                "A spiral-shaped corridor",
                0.0, 0.0, 1.0, 1.0
            ),
        ]
        self.dynamic = [Player(0.5, 0.5, 0.15, 0.15),]
        self.scene = Scene(self.static, self.dynamic)
        self.views = level_views(self)

class Level5(object):
    def __init__(self):
        self.static = [
            # End position
            Goal(0.8, 0.8, 0.2, 0.2),

            # Signal hotspots for topological view
            Signal(0.0, 0.0, 1.0, 1.0),

            # Regions
            Region(
                "I ran out of time. So that's it\n"
                "Congratulations, I suppose. :P\n"
                "Go into the exit to exit the\n"
                "game",
                0.0, 0.0, 1.0, 1.0
            ),

        ]

        self.dynamic = [Player(0.0, 0.0, 0.2, 0.2),]
        self.scene = Scene(self.static, self.dynamic)
        self.views = level_views(self)

