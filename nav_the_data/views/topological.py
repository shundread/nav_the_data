'''This file provides a topological view of the map. Too bad you're not gonna
use it in the game >:^)'''

# Pygame imports
from pygame import draw
from pygame import Rect
from random import randint

# Custom lib imports
from view import View

from ..rectangle import Rectangle
from ..entities.static import Goal, Signal, Wall
from ..entities.player import Player

Yellow = (255, 255, 0)
Green =  (0,   255, 0)
White =  (255, 255, 255)

class TopologicalView(View):
    '''This view displays a topological representation of the map.'''
    def __init__(self, *args, **kargs):
        super(TopologicalView, self).__init__(*args, **kargs)

        self.goal = None
        self.player = None
        self.signals = []
        self.walls = []
        for d in self.scene.dynamic:
            if isinstance(d, Player):
                self.player = d
                break

        for s in self.scene.static:
            if isinstance(s, Signal):
                self.signals.append(s)
            if isinstance(s, Wall):
                self.walls.append(s)
            if isinstance(s, Goal):
                self.goal = s

    def draw_rect(self, rect, color):
        width, height = self.area.size

        x = int(round(width  * rect.left))
        y = int(round(height * rect.top))
        w = int(round(width  * rect.w))
        h = int(round(height * rect.h))

        draw.rect(self.surface, color, Rect(x, y, w, h))

    def draw(self):
        self.surface.fill((30, 0, 0))

        for s in self.signals:
            if Rectangle.intersects(self.player, s):
                self.draw_ok()
                return
        self.draw_broken()

    def draw_broken(self):
        # Add crappy noise, no time to use PIL. :(
        width, height = self.area.size
        for n in range(width+height): #whatever
            x = randint(0, width)
            y = randint(0, height)
            self.surface.set_at((x, y), White)

        g_left   = int(round(width * self.goal.left))
        g_right  = int(round(width * self.goal.right))
        g_top    = int(round(height * self.goal.top))
        g_bottom = int(round(height * self.goal.bottom))
        g_width  = int(round(height * self.goal.w))
        g_height = int(round(height * self.goal.h))

        for n in range(g_width+g_height):
            x = randint(g_left, g_right)
            y = randint(g_top, g_bottom)
            self.surface.set_at((x, y), Green)


    def draw_ok(self):
        self.draw_rect(self.player, Yellow)
        self.draw_rect(self.goal, Green)
        for r in self.walls:
            self.draw_rect(r, White)

