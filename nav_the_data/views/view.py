'''This file defines the base class for all the scene views in the game.'''

from pygame import draw

from ..entities.static import Goal, Wall
from ..entities.dynamic import Dynamic

class View(object):
    '''This class is the base class to all the scene views in the game.'''
    def __init__(self, scene, surface):
        self.scene = scene
        self.surface = surface

    @property
    def area(self):
        return self.surface.get_rect()

    def draw(self):
        '''Draws the view.'''
        draw.rect(self.surface, (255, 255, 255), self.surface.get_rect(), 1)

    def update(self):
        '''Implement the clear/render routine here. Return empty list for
        no updates or a list with one rect for more updates.'''
        pass

