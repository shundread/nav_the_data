from ..entities.dynamic import Dynamic
from ..entities.static import Goal, Wall

class Matcher(object):
    '''This class functions as a filter that selects from a scene a subset of
    elements that the view might be interested in.'''
    def __init__(self, name):
        self.name = name

    @classmethod
    def accepts(self, entity):
        pass

class WallMatcher(object):
    @classmethod
    def accepts(cls, entity):
        return isinstance(entity, Wall)

class GoalMatcher(object):
    @classmethod
    def accepts(cls, entity):
        return isinstance(entity, Goal)

class DynamicMatcher(object):
    @classmethod
    def accepts(cls, entity):
        return isinstance(entity, Dynamic)
