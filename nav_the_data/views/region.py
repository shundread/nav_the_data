'''This file provides a text view for the game. Nice descriptions for sections
of the maze. HAHAHAHA'''

# Pygame imports
from pygame import draw
from pygame import Rect

# Custom lib imports
from view import View

from ..entities.static import Region
from ..entities.player import Player
from ..rectangle import Rectangle

TextColor = (0, 180, 20)
HMargin = 5
WMargin = 5

class RegionView(View):
    def __init__(self, font, *args, **kargs):
        super(RegionView, self).__init__(*args, **kargs)

        self.regions = [s for s in self.scene.static if isinstance(s, Region)]
        self.player = None
        for d in self.scene.dynamic:
            if isinstance(d, Player):
                self.player = d

        self.font = font

    def draw(self):
        self.surface.fill((0, 0, 0))

        current_regions = []
        text = "???"
        for r in self.regions:
            if Rectangle.intersects(self.player, r):
                current_regions.append(r)
                if len(current_regions) > 0:
                    break

        if len(current_regions) == 1:
            text = current_regions[0].text

        lines = text.split("\n")
        for n, text in enumerate(lines):
            rendered = self.font.render(text, True, TextColor)
            y = n * self.font.get_linesize()
            self.surface.blit(rendered, (WMargin, HMargin + y))
