'''This file provides histogram-based views of the level. :P :P :P'''

# Pygame imports
import pygame
from pygame import draw
from pygame import Rect

# Custom lib imports
from view import View
from ..rectangle import Rectangle

class HistogramView(View):
    '''This class presents a histogram view of a scene.'''
    def __init__(self, name, matcher, n_samples, *args, **kargs):
        super(HistogramView, self).__init__(*args, **kargs)

        # Title stuff
        self.font = pygame.font.SysFont("arial", 20)
        self.name = name
        title_w = self.surface.get_width()
        title_h = self.font.get_linesize()
        margin = 5
        main_w = title_w - 2*margin
        main_h = self.surface.get_height() - title_h - 2*margin
        self.t_surface = self.surface.subsurface(0, 0, title_w, title_h)
        self.surface = self.surface.subsurface(margin, title_h, main_w, main_h)
        self.title = self.font.render(self.name, True, (255, 255, 255))

        self.matcher = matcher

        self.n_samples = n_samples
        self.sampler_size = 1.0 / n_samples

        self.rectangles = self.get_rectangles()

    def draw(self):
        super(HistogramView, self).draw()
        self.t_surface.blit(self.title, (0, 0))

    def get_rectangles(self):
        result = []
        for e in self.scene.entities:
            if self.matcher.accepts(e):
                result.append(e)
        return result

    def get_sampler(self, index):
        '''Returns a sampler rectangle that indicates the area of the level
        being sampled by the given index. Must be implemented by subclass.'''
        pass

    def intensity_at(self, index):
        '''Calculates the intensity of the histogram at a given index along its
        axis. The intensity is the total area of the sampler covered by the
        level's chosen rectangles in relation to the sampler area and should be
        in the range of [0.0, 1.0], should the level be constructed properly.'''
        sampler = self.get_sampler(index)
        result = 0.0

        for r in self.rectangles:
            intersection = Rectangle.intersection(sampler, r)
            if intersection:
                result += intersection.area

        return result / sampler.area

class HorizontalHistogramView(HistogramView):
    '''The horizontal implementation of the HistogramView. It takes column
    slices of the level.'''
    def get_sampler(self, index):
        width = self.sampler_size
        return Rectangle(index * width, 0, width, 1)

    def draw(self):
        self.surface.fill((0, 30, 0))
        super(HorizontalHistogramView, self).draw()

        width, height = self.area.size
        column_w = float(width) / self.n_samples

        points = [(int(round(column_w * i)),
                   int(round(self.intensity_at(i) * height)))
                   for i in range(self.n_samples)]

        draw.lines(self.surface, (255, 255, 255), False, points, 5)

class VerticalHistogramView(HistogramView):
    '''The vertical implementation of the HistogramView. It takes row slices of
    the level.'''
    def get_sampler(self, index):
        height = self.sampler_size
        return Rectangle(0, index * height, 1, height)

    def draw(self):
        self.surface.fill((0, 0, 30))
        super(VerticalHistogramView, self).draw()

        width, height = self.area.size
        row_h = float(height) / self.n_samples

        points = [(int(round(self.intensity_at(i) * width)),
                   int(round(row_h * i)))
                   for i in range(self.n_samples)]

        draw.lines(self.surface, (255, 255, 255), False, points, 5)

