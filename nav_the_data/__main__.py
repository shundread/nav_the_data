# Pygame imports
import pygame
from pygame import display, font, time
from pygame import Rect

# Custom lib imports
from engine import Engine
from level import Level1, Level2, Level3, Level4, Level5

def draw():
    pass

def init():
    pygame.init()
    pygame.display.set_mode((800,800))

def main():
    init()

    for level in [Level1, Level2, Level3, Level4, Level5]:
        while True:
            engine = Engine(level())
            engine.loop()
            if engine.quit:
                return
            if engine.success:
                break
