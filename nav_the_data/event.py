from pygame import USEREVENT
from pygame.event import Event

GOAL_REACHED = USEREVENT
PLAYER_DIED  = USEREVENT + 1

def GoalReached():
    return Event(GOAL_REACHED)

def PlayerDied():
    return Event(PLAYER_DIED)

