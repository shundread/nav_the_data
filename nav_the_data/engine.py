'''This file defines the main engine of the game and auxiliary classes'''

# Pygame imports
import pygame
from pygame.time import Clock

# Custom imports
from rectangle import Rectangle
from event import GOAL_REACHED, PLAYER_DIED

class Scene(object):
    def __init__(self, static, dynamic):
        self.static = static
        self.dynamic = dynamic

    @property
    def entities(self):
        return self.static + self.dynamic

    def update(self, events, dt):
        # Update dynamic objects' positions and test collision between said
        # dynamic objects ant the walls.
        for d in self.dynamic:
            d.update(events, dt)

            # We update one component's position at a time, to make for simple
            # and lazy collision handling.
            if d.dx:
                d.x += d.dx
                d.left = max(d.left, 0)
                d.right = min(d.right, 1)
                for s in self.static:
                    if Rectangle.intersects(s, d):
                        # Fix the dynamic object's position
                        if s.solid:
                            if d.dx > 0:
                                d.right = s.left
                            elif d.dx < 0:
                                d.left = s.right

                        # Signal the dynamic object about the collision
                        d.collide(s)

            # It's a bit of a crappy method and it favours one type of collision
            # over another, but we have so few objects at any time that it
            # doesn't make much of a difference in performance.
            if d.dy:
                d.y += d.dy
                d.top = max(d.top, 0.0)
                d.bottom = min(d.bottom, 1.0)
                for s in self.static:
                    if Rectangle.intersects(s, d):
                        # Fix the dynamic object's position
                        if s.solid:
                            if d.dy > 0:
                                d.bottom = s.top
                            elif d.dy < 0:
                                d.top = s.bottom

                        # Signal the dynamic object about the collision
                        d.collide(s)

        # Since collision between dynamic objects will be handled differently
        # (you get a reward, damaged or lose), there's not gonna be a reason for
        # accurate fixing of the dynamic objects' positions, so we don't do per
        # movement component test. Also, position has already been updated, so
        # no need for further position updates.
        for n, a in enumerate(self.dynamic):
            for b in self.dynamic[n+1:]:
                if Rectangle.intersects(a, b):
                    a.collide(b)
                    b.collide(a)

class Engine(object):
    def __init__(self, level):
        self.scene = level.scene
        self.views = level.views
        self.quit = False
        self.success = False

    def update(self, events, dt):
        self.scene.update(events, dt)
        for v in self.views:
            #v.update(events, dt)
            v.update()

    def draw(self):
        for v in self.views:
            v.draw()

    def loop(self):
        pygame.event.set_blocked(None)
        pygame.event.set_allowed([
            pygame.QUIT,
            pygame.KEYDOWN,
            pygame.MOUSEBUTTONDOWN,
            GOAL_REACHED,
            PLAYER_DIED
        ])
        clock = Clock()
        dt = 0.0
        while True:
            if pygame.event.get(pygame.QUIT):
                self.quit = True
                return
            if pygame.event.get(GOAL_REACHED):
                self.success = True
                return
            if pygame.event.get(PLAYER_DIED):
                return

            events = pygame.event.get([pygame.KEYDOWN, pygame.MOUSEBUTTONDOWN])
            self.update(events, dt)
            self.draw()

            pygame.display.flip()
            dt = clock.tick(60) / 1000.0

